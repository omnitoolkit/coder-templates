## 1.0.0 (2025-02-20)


### Features

* add custom image to docker-general template ([6182b77](https://gitlab.com/omnitoolkit/coder-templates/commit/6182b774a1d94500950ab7e33bebe9d35672aa46))
* add docker-devcontainer-starter template ([9128861](https://gitlab.com/omnitoolkit/coder-templates/commit/9128861a1a64bb84560e5544ad2e96bb5fe317b0))
* add docker-general template ([8fb4e7f](https://gitlab.com/omnitoolkit/coder-templates/commit/8fb4e7fc0681a319098b124b130e93a52dcd814c))
* add scripts and docker-starter template ([cbc2e34](https://gitlab.com/omnitoolkit/coder-templates/commit/cbc2e349ca8cd95012c6fdb6c509bd2027072cdb))
* change typo in tf-check script ([abad875](https://gitlab.com/omnitoolkit/coder-templates/commit/abad875a2a3415303375553fd1d1f5dfebed5fd2))
* **docker-general:** add ci job to build image ([2f02871](https://gitlab.com/omnitoolkit/coder-templates/commit/2f02871145401c9da2f20c24056e7607a1f556fe))
* **docker-general:** set autostart for coder and code-server ([fce423d](https://gitlab.com/omnitoolkit/coder-templates/commit/fce423da6aeab508b224c0192c9a79c6fb77a8ce))
* move templates to subdirectory ([05796a3](https://gitlab.com/omnitoolkit/coder-templates/commit/05796a34da7fefd37ee198024437ebe0e9a0fbd3))
* update coder cli and starter templates to v2.19.0 ([ef44178](https://gitlab.com/omnitoolkit/coder-templates/commit/ef44178c9436d2f8212fdb31e8c212c62e889910))
* update docker image for build job ([b5e5638](https://gitlab.com/omnitoolkit/coder-templates/commit/b5e56383ffda9a655364558a3b703fa399e2f712))
