#
################################################################################
#
# Install Apache Directory Studio from the official sources.
#
# https://directory.apache.org/studio/download/download-linux.html
#
ARG APACHE_DIRECTORY_STUDIO_AUTOSTART=false
ARG APACHE_DIRECTORY_STUDIO_VERSION=2.0.0.v20210717-M17
#
RUN \
  apt-get update && \
  apt-get install -y --no-install-recommends \
    libgtk-3-0 \
    openjdk-17-jdk \
    && \
  rm -rf /var/lib/apt/lists/* && \
  curl -fsSL "https://dlcdn.apache.org/directory/studio/${APACHE_DIRECTORY_STUDIO_VERSION}/ApacheDirectoryStudio-${APACHE_DIRECTORY_STUDIO_VERSION}-linux.gtk.x86_64.tar.gz" \
    -o /tmp/apache-directory-studio.tar.gz && \
  tar -C /opt -xzf /tmp/apache-directory-studio.tar.gz && \
  rm -rf /tmp/apache-directory-studio.tar.gz
#
COPY <<EOF /etc/supervisor/conf.d/apache-directory-studio.conf
[program:apache-directory-studio]
autorestart = true
autostart = ${APACHE_DIRECTORY_STUDIO_AUTOSTART}
command = /opt/ApacheDirectoryStudio/ApacheDirectoryStudio
stderr_logfile = /dev/stderr
stderr_logfile_maxbytes = 0
stdout_logfile = /dev/stdout
stdout_logfile_maxbytes = 0
EOF
