resource "coder_app" "apache-directory-studio" {
  agent_id     = coder_agent.main.id
  icon         = "https://directory.apache.org/studio/screenshots.data/studio-icon_128x128.png"
  display_name = "Apache Directory Studio"
  open_in      = "tab"
  share        = "owner"
  slug         = "apache-directory-studio"
  subdomain    = true
  url          = "http://localhost:6080/?autoconnect=true&resize=scale&path=@${data.coder_workspace_owner.me.name}/${data.coder_workspace.me.name}.main/apps/apache-directory-studio/websockify"
}
