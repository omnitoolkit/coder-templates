# change user-data-dir before executing
COPY <<EOF /usr/share/code-server/User/settings.json
{
  "editor.indentSize": 2,
}
EOF
COPY <<EOF /usr/share/code-server/extensions/extensions.list
EditorConfig.EditorConfig@0.16.4
EOF
RUN \
  cat /usr/share/code-server/extensions/extensions.list | xargs -n 1 \
    code-server \
    --config /dev/null \
    --disable-telemetry \
    --user-data-dir /usr/share/code-server \
    --install-extension
