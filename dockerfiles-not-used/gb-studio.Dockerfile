#
################################################################################
#
# Install GB Studio from the official sources. The version defaults to the latest and can
# be set via build arguments.
#
# https://www.gbstudio.dev/docs/installation
#
ARG GB_STUDIO_AUTOSTART=false
ARG GB_STUDIO_VERSION
#
RUN \
  apt-get update && \
  apt-get install -y --no-install-recommends \
    build-essential \
    libnotify4 \
    xdg-utils \
    kde-cli-tools \
    trash-cli \
    libatk-bridge2.0-dev \
    libatk1.0-dev \
    libglib2.0-bin \
    libgtk-3-dev \
    libnss3-dev \
    && \
  rm -rf /var/lib/apt/lists/* && \
  export GB_STUDIO_VERSION=${GB_STUDIO_VERSION:-$(curl -s "https://api.github.com/repos/chrismaltby/gb-studio/releases/latest" | jq -r .tag_name | sed "s/^v//g")} && \
  curl -fsSL "https://github.com/chrismaltby/gb-studio/releases/download/v${GB_STUDIO_VERSION}/gb-studio-linux-debian.deb" \
    -o /tmp/gb-studio-linux-debian.deb && \
  dpkg --force-all -i /tmp/gb-studio-linux-debian.deb && \
  rm -rf /tmp/gb-studio-linux-debian.deb
#
COPY <<EOF /etc/supervisor/conf.d/gb-studio.conf
[program:gb-studio]
autorestart = true
autostart = ${GB_STUDIO_AUTOSTART}
command = /usr/bin/gb-studio --no-sandbox --disable-gpu-sandbox
stderr_logfile = /dev/stderr
stderr_logfile_maxbytes = 0
stdout_logfile = /dev/stdout
stdout_logfile_maxbytes = 0
EOF
