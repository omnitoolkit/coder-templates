#
################################################################################
#
# Install go from the official sources. The version defaults to the latest and can be set
# via build arguments.
#
# https://go.dev/dl
# https://go.dev/doc/install
# https://go.dev/doc/devel/release
#
ARG GO_VERSION
#
RUN \
  export GO_VERSION=${GO_VERSION:-$(curl -s "https://go.dev/dl/?mode=json" | jq -r ".[0].version" | sed "s/go//g")} && \
  curl -fsSL "https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz" \
    -o /tmp/go.tar.gz && \
  tar -C /usr/local -xzf /tmp/go.tar.gz && \
  rm -rf /tmp/go.tar.gz && \
  \
  mkdir -p /etc/zsh && \
  echo "export PATH=\$PATH:/usr/local/go/bin" | tee -a /etc/profile /etc/bash.bashrc /etc/zsh/zshrc && \
  . /etc/profile && \
  go version
#
################################################################################
#
# Install miscellaneous go modules.
#
# https://pkg.go.dev/
#
RUN \
  . /etc/profile && \
  go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest && \
  go install github.com/mbenkmann/goformat/goformat@latest && \
  go install golang.org/x/tools/cmd/godoc@latest && \
  go install golang.org/x/tools/cmd/goimports@latest && \
  go install golang.org/x/tools/gopls@latest && \
  go install mvdan.cc/gofumpt@latest
