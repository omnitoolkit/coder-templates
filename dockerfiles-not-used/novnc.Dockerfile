#
################################################################################
#
# Install noVNC and other related programs from the system repositories.
#
# https://github.com/novnc/noVNC
# https://github.com/theasp/docker-novnc
# http://localhost:6080/vnc.html?autoconnect=true&resize=scale&path=path/to/websockify
#
#
ARG NOVNC_AUTOSTART=false
#
ENV DISPLAY=:0.0
#
RUN \
  apt-get update && \
  DEBIAN_FRONTEND=noninteractive \
  apt-get install -y --no-install-recommends \
    fluxbox \
    novnc \
    x11vnc \
    xvfb \
    && \
  rm -rf /var/lib/apt/lists/* && \
  cp /usr/share/novnc/vnc.html /usr/share/novnc/index.html
#
COPY <<EOF /etc/supervisor/conf.d/novnc.conf
[program:fluxbox]
autorestart = true
autostart = ${NOVNC_AUTOSTART}
command = fluxbox
stderr_logfile = /dev/stderr
stderr_logfile_maxbytes = 0
stdout_logfile = /dev/stdout
stdout_logfile_maxbytes = 0
#
[program:websockify]
autorestart = true
autostart = ${NOVNC_AUTOSTART}
command = websockify --web /usr/share/novnc 6080 localhost:5900
stderr_logfile = /dev/stderr
stderr_logfile_maxbytes = 0
stdout_logfile = /dev/stdout
stdout_logfile_maxbytes = 0
#
[program:x11vnc]
autorestart = true
autostart = ${NOVNC_AUTOSTART}
command = x11vnc -forever -shared
stderr_logfile = /dev/stderr
stderr_logfile_maxbytes = 0
stdout_logfile = /dev/stdout
stdout_logfile_maxbytes = 0
#
[program:xvfb]
autorestart = true
autostart = ${NOVNC_AUTOSTART}
command = Xvfb :0 -screen 0 1920x1080x24 -listen tcp -ac
stderr_logfile = /dev/stderr
stderr_logfile_maxbytes = 0
stdout_logfile = /dev/stdout
stdout_logfile_maxbytes = 0
EOF
#
EXPOSE 6080
