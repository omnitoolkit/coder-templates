#
################################################################################
#
# Install rgbds from the official sources. The version defaults to the latest
# and can be set via build arguments.
#
# https://rgbds.gbdev.io/install/source
#
ARG RGBDS_VERSION
#
RUN \
  apt-get update && \
  apt-get install -y --no-install-recommends \
    bison \
    build-essential \
    libpng-dev \
    pkg-config \
    && \
  rm -rf /var/lib/apt/lists/* && \
  \
  export RGBDS_VERSION=${RGBDS_VERSION:-$(curl -s "https://api.github.com/repos/gbdev/rgbds/releases/latest" | jq -r .tag_name | sed "s/^v//g")} && \
  curl -fsSL "https://github.com/gbdev/rgbds/releases/download/v${RGBDS_VERSION}/rgbds-${RGBDS_VERSION}.tar.gz" \
    -o /tmp/rgbds.tar.gz && \
  tar -C /tmp -xzf /tmp/rgbds.tar.gz && \
  cd /tmp/rgbds && \
  make install && \
  rm -rf /tmp/rgbds.tar.gz /tmp/rgbds && \
  rgbasm --version && \
  rgbfix --version && \
  rgbgfx --version && \
  rgblink --version
#
# vscode extensions:
# donaldhays.rgbds-z80
# emulicious.emulicious-debugger
#
# emulators:
# https://emulicious.net/download/emulicious/
# default-jdk
