#
################################################################################
#
# Install vscode-server from the official sources. The version defaults to the latest and
# can be set via build arguments.
#
# https://github.com/microsoft/vscode/releases
#
ARG VSCODE_SERVER_AUTOSTART=false
ARG VSCODE_SERVER_VERSION
#
RUN \
  export VSCODE_SERVER_VERSION=${VSCODE_SERVER_VERSION:-$(curl -s "https://api.github.com/repos/microsoft/vscode/releases/latest" | jq -r .tag_name | sed "s/^v//g")} && \
  export VSCODE_SERVER_COMMIT=$(curl -fsSL "https://api.github.com/repos/microsoft/vscode/git/ref/tags/${VSCODE_SERVER_VERSION}" | jq -r .object.sha) && \
  curl -fsSL "https://vscode.download.prss.microsoft.com/dbazure/download/stable/${VSCODE_SERVER_COMMIT}/vscode-server-linux-x64-web.tar.gz" \
    -o /tmp/vscode-server.tar.gz && \
  mkdir -p /usr/lib/vscode-server && \
  tar -C /usr/lib/vscode-server -xzf /tmp/vscode-server.tar.gz --no-same-owner --strip-components 1 && \
  ln -fs /usr/lib/vscode-server/bin/code-server /usr/bin/vscode-server && \
  vscode-server --version && \
  rm -rf /root/.vscode-server/
#
COPY <<EOF /etc/supervisor/conf.d/vscode-server.conf
[program:vscode-server]
autorestart = true
autostart = ${VSCODE_SERVER_AUTOSTART}
command = vscode-server \
  --accept-server-license-terms \
  --disable-telemetry \
  --disable-workspace-trust \
  --host 0.0.0.0 \
  --log info \
  --port 8081 \
  --without-connection-token
stderr_logfile = /dev/stderr
stderr_logfile_maxbytes = 0
stdout_logfile = /dev/stdout
stdout_logfile_maxbytes = 0
EOF
#
EXPOSE 8081
