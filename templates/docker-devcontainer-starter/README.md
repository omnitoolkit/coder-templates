---
description: Remote Development on Docker Containers (with Devcontainers)
display_name: docker-devcontainer-starter
icon: /icon/docker-white.svg
---

# docker-devcontainer-starter

source: https://github.com/coder/coder/blob/v2.19.0/examples/templates/docker-devcontainer/main.tf
