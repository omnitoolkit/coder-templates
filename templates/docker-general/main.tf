terraform {
  required_providers {
    coder = {
      # https://registry.terraform.io/providers/coder/coder/2.1.0/docs
      source  = "coder/coder"
      version = "2.1.0"
    }
    docker = {
      # https://registry.terraform.io/providers/kreuzwerker/docker/3.0.2/docs
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

# TODO: enable remote docker host
# variable "DOCKER_HOST" {
#   default     = ""
#   description = "(Optional) Docker host URI"
#   type        = string
# }

variable "DOCKER_NETWORK" {
  default     = "docker_default"
  description = "The name of the docker network that the docker container will use."
  type        = string
}

variable "DOCKER_VOLUMES_DIR" {
  default     = "/srv/docker/volumes/coder-workspaces"
  description = "The directory on the docker host where the volumes of the workspaces are stored."
  type        = string
}

variable "GIT_REPO_DIR" {
  default     = "/root/repo"
  description = "The directory on the docker container where the git repository will be cloned."
  type        = string
}

variable "GIT_REPO_URL" {
  default     = "https://gitlab.com/omnitoolkit/ansible-collection-general.git"
  description = "The URL of the git repository that will be cloned."
  type        = string
}

provider "docker" {
  # TODO: enable remote docker host
  # host     = var.DOCKER_HOST != "" ? var.DOCKER_HOST : null
  # ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null"]

  # TODO: configure private registry
  # registry_auth {
  #   address  = "registry.example.com"
  #   password = "password"
  #   username = "username"
  # }
}

# data source to pre-authenticate external services in a workspace
data "coder_external_auth" "gitlab" {
  id = "gitlab"
}

# data source to get information about the Coder provisioner
data "coder_provisioner" "me" {}
# data source to get information for the active workspace build
data "coder_workspace" "me" {}
# data source to get information about the workspace owner
data "coder_workspace_owner" "me" {}

resource "coder_agent" "main" {
  arch               = data.coder_provisioner.me.arch
  connection_timeout = 30
  dir                = "/root"
  os                 = data.coder_provisioner.me.os
  startup_script     = <<-EOT
    set -eu

    if [ ! -f ~/.init_done ]; then
      cp -rT /etc/skel ~
      touch ~/.init_done
    fi

    mkdir -p ~/.ssh
    echo "${data.coder_workspace_owner.me.ssh_private_key}" > ~/.ssh/id_ed25519
    echo "${data.coder_workspace_owner.me.ssh_public_key}" > ~/.ssh/id_ed25519.pub
    chmod 0700 ~/.ssh
    chmod 0600 ~/.ssh/id_ed25519
    chmod 0600 ~/.ssh/id_ed25519.pub

    git config --global credential.helper '!f() { sleep 1; echo "username=token"; echo "password=$${GITLAB_ACCESS_TOKEN}"; }; f'

    # mkdir -p ${var.GIT_REPO_DIR}
    # git clone ${var.GIT_REPO_URL} ${var.GIT_REPO_DIR} || true
    # cd ${var.GIT_REPO_DIR}
    # git pull
  EOT

  # TODO: use dotfiles
  # env = {
  #   GIT_AUTHOR_NAME     = coalesce(data.coder_workspace_owner.me.full_name, data.coder_workspace_owner.me.name)
  #   GIT_AUTHOR_EMAIL    = "${data.coder_workspace_owner.me.email}"
  #   GIT_COMMITTER_NAME  = coalesce(data.coder_workspace_owner.me.full_name, data.coder_workspace_owner.me.name)
  #   GIT_COMMITTER_EMAIL = "${data.coder_workspace_owner.me.email}"
  # }
  display_apps {
    port_forwarding_helper = false
    ssh_helper             = false
    vscode                 = false
    web_terminal           = false
  }

  metadata {
    display_name = "CPU Usage"
    key          = "0_cpu_usage"
    script       = "coder stat cpu"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "RAM Usage"
    key          = "1_ram_usage"
    script       = "coder stat mem"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "Home Disk"
    key          = "3_home_disk"
    script       = "coder stat disk --path $${HOME}"
    interval     = 60
    timeout      = 1
  }

  metadata {
    display_name = "CPU Usage (Host)"
    key          = "4_cpu_usage_host"
    script       = "coder stat cpu --host"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "Memory Usage (Host)"
    key          = "5_mem_usage_host"
    script       = "coder stat mem --host"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "Load Average (Host)"
    key          = "6_load_host"
    script       = <<EOT
      echo "`cat /proc/loadavg | awk '{ print $1 }'` `nproc`" | awk '{ printf "%0.2f", $1/$2 }'
    EOT
    interval     = 60
    timeout      = 1
  }

  metadata {
    display_name = "Swap Usage (Host)"
    key          = "7_swap_host"
    script       = <<EOT
      free -b | awk '/^Swap/ { printf("%.1f/%.1f", $3/1024.0/1024.0/1024.0, $2/1024.0/1024.0/1024.0) }'
    EOT
    interval     = 10
    timeout      = 1
  }
}

resource "coder_app" "code-server" {
  agent_id     = coder_agent.main.id
  icon         = "${data.coder_workspace.me.access_url}/icon/code.svg"
  display_name = "VS Code Web"
  open_in      = "tab"
  share        = "owner"
  slug         = "code-server"
  subdomain    = true
  url          = "http://localhost:8080/?folder=${coder_agent.main.dir}" # ${var.GIT_REPO_DIR}

  healthcheck {
    interval  = 5
    threshold = 6
    url       = "http://localhost:8080/healthz"
  }
}

resource "docker_image" "main" {
  name = "coder--${lower(data.coder_workspace_owner.me.name)}--${lower(data.coder_workspace.me.name)}--image"

  triggers = {
    dir_sha1 = sha1(join("", [for f in fileset(path.module, "build/*") : filesha1(f)]))
  }

  build {
    context = "./build"
  }
}

resource "docker_network" "main" {
  count = data.coder_workspace.me.start_count
  name  = "coder--${lower(data.coder_workspace_owner.me.name)}--${lower(data.coder_workspace.me.name)}--network"
}

resource "docker_container" "dind" {
  count      = data.coder_workspace.me.start_count
  entrypoint = ["dockerd", "-H", "tcp://0.0.0.0:2375"]
  image      = "docker.io/library/docker:27.4.1-dind"
  name       = "coder--${lower(data.coder_workspace_owner.me.name)}--${lower(data.coder_workspace.me.name)}--dind"
  privileged = true

  networks_advanced {
    name = docker_network.main[0].name
  }
}

resource "docker_container" "workspace" {
  count      = data.coder_workspace.me.start_count
  hostname   = lower(data.coder_workspace.me.name)
  image      = docker_image.main.name
  name       = "coder--${lower(data.coder_workspace_owner.me.name)}--${lower(data.coder_workspace.me.name)}--workspace"
  privileged = true

  env = [
    "CODER_AGENT_TOKEN=${coder_agent.main.token}",
    "CODER_AGENT_URL=${data.coder_workspace.me.access_url}",
    "DOCKER_HOST=${docker_container.dind[0].name}:2375",
    "GITLAB_ACCESS_TOKEN=${data.coder_external_auth.gitlab.access_token}"
  ]

  networks_advanced {
    name = docker_network.main[0].name # var.DOCKER_NETWORK
  }

  labels {
    label = "coder.owner"
    value = data.coder_workspace_owner.me.name
  }

  labels {
    label = "coder.owner_id"
    value = data.coder_workspace_owner.me.id
  }

  labels {
    label = "coder.workspace_id"
    value = data.coder_workspace.me.id
  }

  labels {
    label = "coder.workspace_name"
    value = data.coder_workspace.me.name
  }

  volumes {
    container_path = coder_agent.main.dir
    host_path      = "${var.DOCKER_VOLUMES_DIR}/coder--${lower(data.coder_workspace_owner.me.name)}--${lower(data.coder_workspace.me.name)}--volume"
  }
}
