---
description: Remote Development on Docker Containers
display_name: docker-starter
icon: /icon/docker-white.svg
---

# docker-starter

source: https://github.com/coder/coder/blob/v2.19.0/examples/templates/docker/main.tf
